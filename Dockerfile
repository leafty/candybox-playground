FROM node:lts

EXPOSE 5000

ENV APP_PORT=5000

RUN mkdir -p /app \
 && chown -R node:node /app

USER node

WORKDIR /app

COPY --chown=node:node package.json yarn.lock ./

RUN yarn install --production --frozen-lockfile

COPY --chown=node:node public public
COPY --chown=node:node .next .next

ARG APP_VERSION
ENV APP_VERSION=${APP_VERSION}

CMD [ "yarn", "start", "--port", "5000" ]
