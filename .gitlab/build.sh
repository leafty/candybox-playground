#!/bin/bash -e

# build stage script
# Customized from https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/-/blob/master/src/build.sh

if ! docker info &>/dev/null; then
  if [ -z "$DOCKER_HOST" ] && [ "$KUBERNETES_PORT" ]; then
    export DOCKER_HOST='tcp://localhost:2375'
  fi
fi

if [[ -n "$CI_REGISTRY" && -n "$CI_REGISTRY_USER" ]]; then
  echo "Logging to GitLab Container Registry with CI credentials..."
  echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"
fi

image_previous="$CI_APPLICATION_REPOSITORY:$CI_COMMIT_BEFORE_SHA"
image_tagged="$CI_APPLICATION_REPOSITORY:$CI_APPLICATION_TAG"
image_latest="$CI_APPLICATION_REPOSITORY:latest"
image_main="$CI_REGISTRY_IMAGE/main:latest"

if [[ -n "${DOCKERFILE_PATH}" ]]; then
  echo "Building Dockerfile-based application using '${DOCKERFILE_PATH}'..."
else
  export DOCKERFILE_PATH="Dockerfile"

  if [[ -f "${DOCKERFILE_PATH}" ]]; then
    echo "Building Dockerfile-based application..."
  else
    echo "Building Heroku-based application using gliderlabs/herokuish docker image..."
    erb -T - /build/Dockerfile.erb > "${DOCKERFILE_PATH}"
  fi
fi

if [[ ! -f "${DOCKERFILE_PATH}" ]]; then
  echo "Unable to find '${DOCKERFILE_PATH}'. Exiting..." >&2
  exit 1
fi

docker buildx build \
  --platform linux/arm64/v8 \
  --cache-from "type=registry,ref=${image_previous}" \
  --cache-from "type=registry,ref=${image_latest}" \
  --cache-from "type=registry,ref=${image_main}" \
  -f "${DOCKERFILE_PATH}" \
  $AUTO_DEVOPS_BUILD_IMAGE_EXTRA_ARGS \
  --build-arg APP_VERSION="${CI_COMMIT_SHA}" \
  --tag "${image_tagged}" \
  --tag "${image_latest}" \
  --progress=plain \
  --push \
  .
