# Customized Auto DevOps CI/CD pipeline
# Base template: https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml

variables:
  AUTO_DEPLOY_IMAGE_VERSION: 'v2.12.0'

  POSTGRES_ENABLED: "false"

  DOCKER_DRIVER: overlay2

  ROLLOUT_RESOURCE_TYPE: deployment

  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501


stages:
  - build-node
  - build
  - test
  - deploy  # dummy stage to follow the template guidelines
  - review
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - cleanup

build-node:
  stage: build-node
  image: node:lts
  before_script:
    - node -v  # Print out node version for debugging
    - yarn install --frozen-lockfile
  script:
    - yarn build
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - node_modules/
      - .next/cache/
  artifacts:
    paths:
      - .next/
    expire_in: 1 week
  rules:
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'

build:
  stage: build
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image:v1.4.0"
  services:
    - docker:dind
  script:
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - ./.gitlab/build.sh
  rules:
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'

.auto-deploy:
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:${AUTO_DEPLOY_IMAGE_VERSION}"
  dependencies: []
  before_script:
    - export KUBE_INGRESS_BASE_DOMAIN="candybox"
    - export KUBE_NAMESPACE="$CI_PROJECT_NAME-$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG"
    - echo "$KUBE_INGRESS_BASE_DOMAIN"
    - echo "$KUBE_NAMESPACE"

review:
  extends: .auto-deploy
  stage: review
  tags:
    - candybox
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy persist_environment_url
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_PROJECT_ID-$CI_ENVIRONMENT_SLUG.candybox
    on_stop: stop_review
  artifacts:
    paths: [environment_url.txt, tiller.log]
    when: always
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$REVIEW_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
      when: manual

stop_review:
  extends: .auto-deploy
  stage: cleanup
  variables:
    GIT_STRATEGY: none
  tags:
    - candybox
  script:
    - auto-deploy initialize_tiller
    - auto-deploy delete
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: never
    - if: '$REVIEW_DISABLED'
      when: never
    - if: '$CI_COMMIT_TAG || $CI_COMMIT_BRANCH'
      when: manual

.production: &production_template
  extends: .auto-deploy
  stage: production
  tags:
    - candybox
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy delete canary
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: http://candybox
  artifacts:
    paths: [environment_url.txt, tiller.log]
    when: always

production_manual:
  <<: *production_template
  allow_failure: false
  rules:
    - if: '$CI_DEPLOY_FREEZE != null'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_ENABLED'
      when: never
    - if: '$INCREMENTAL_ROLLOUT_MODE'
      when: never
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      when: manual
