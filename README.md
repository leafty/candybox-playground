# Candybox-Playground

This is a simple project demonstrating how to use GitLab CI/CD pipelines
to deploy a web app to a microk8s cluster hosted on a Raspberry Pi 4 model B (8 GiB).

## Contents

* A basic web app created with the [NextJS framework](https://nextjs.org).
* A Dockerfile to package the web app into a container image which can run on arm64.
* A CI/CD pipeline to deploy the web app to the microk8s cluster.

## Read More

Read more on my [Hashnode post](https://hashnode.com/preview/6148a623699fe76cd3484c0c).
